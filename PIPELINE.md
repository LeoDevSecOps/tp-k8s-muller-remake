# Pipeline setup and workflow

## General
Notre CI/CD comporte 3 stages:
```yml
stages:
  - build
  - push
  - deploy
```

Nos artefacts seront conservé 6h dans le dossier archive nommé tel que : `archive:${ARTIFACT_NAME}:${CI_JOB_ID}`

## Variables
CI_REGISTRY_USER=leodevsecops
CI_REGISTRY_PASSWORD=********
CI_REGISTRY=docker.io
CI_REGISTRY_IMAGE=index.docker.io/leodevsecops/tp-k8s-muller
kube_config=(kubernetes cluster config, base64 encoded)

## Build & push
Docker images are build with buildah and pushed to dockerhub using buildah.

## Deploy
Pour le deploiment de l'application nous nous connectons au cluster grace à la config K8S stocker dans la variable `kube_config` qui est en base64.
La mise en place et update du cluster se fera via helm.
