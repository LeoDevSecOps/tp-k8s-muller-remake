FROM node:14.15-alpine

# Commenté car pas utile normalement
# ARG NODE_ENV=production
# ENV NODE_ENV=${NODE_ENV}

WORKDIR /node_app

COPY package*.json ./
COPY app/ ./app/

RUN npm install

CMD ["npm", "start"]
